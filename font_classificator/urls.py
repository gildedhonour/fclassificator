from django.contrib import admin
from django.conf import settings
from django.conf.urls import patterns, url, include

from app import views

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', views.index),
    url(r'', include('app.urls'))
]