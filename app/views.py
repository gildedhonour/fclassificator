from django.shortcuts import render
from django.http import HttpResponse
import json
from django.template.defaulttags import register
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q
from django.conf import settings

import models

json_ok = JsonResponse({'status': '200', 'message': 'ok'})
page_size = 20

def index(request):
    params = {}
    for x, def_val in {'positive': True, 'predicted': False, 'corrected': True, 'prediction_sort': True, 'descreasing_sort': True}.items():
        if request.GET.has_key(x):
            params[x] = True if request.GET[x] == 'true' else False
        else:
            params[x] = def_val
    
    data = models.FontFamily.objects.all()

    # Filtering

    # 1 Class and positive
    # todo refactor
    font_class = None
    if request.GET.has_key('font_class'):
        font_class = request.GET['font_class']
        if font_class == '1': # no TK class
            data = data.filter(font_classes__name=None).distinct()
        else:
            if font_class != '0':
                fc = models.FontClass.objects.get(name=font_class, is_original=True)
                params['font_class'] = {'id': fc.id, 'name': fc.name}
                if params['corrected']:

                    # todo - remove?
                    if params['positive']:
                        # fonts that have font correction == params['font_class']
                        data1 = filter(lambda x: x.font_class_correction() != None and x.font_class_correction().name == fc.name, data)
                    else:
                        # fonts that are corrected to not be the font_class
                        data1 = filter(lambda x: x.font_class_correction() != None and x.font_class_correction().name != fc.name, data)
                    
                    data1 = models.FontFamily.objects.filter(id__in=map(lambda x: x.id, data1)).distinct()
    #                data1 = data1.filter(fontclassvalue__value=params['positive']).distinct()
                    
                    # fonts that don't have font correction and belong to params['font_class']
                    data2a = data.filter(font_classes__name=font_class).filter(fontclassvalue__value=params['positive'], fontclassvalue__font_class=fc).distinct()
                    data2b = filter(lambda x: x.font_class_correction() == None, data2a)
    #                data2a = filter(lambda x: x.font_class_correction() == None, data)
    #                data2b = models.FontFamily.objects.filter(id__in=map(lambda x: x.id, data2a))
    #                data2b = myFamilyFilter(map(lambda x: x.id, data2a))
    #                data2c = data2b.filter(font_classes__name=font_class).filter(fontclassvalue__value=params['positive'], fontclassvalue__font_class=fc).distinct()

                    ids1 = map(lambda x: x.id, data1)
                    ids2 = map(lambda x: x.id, data2b)
                    ids_merged = ids1 + ids2
                    
                    # todo - hack - get all ids and get the font families again
                    data = models.FontFamily.objects.filter(id__in=ids_merged)
                else:
                    data = data.filter(font_classes__name=font_class).filter(fontclassvalue__value=params['positive'], fontclassvalue__font_class=fc).distinct()
 



    # 3 Unapproved only?
    if request.GET.has_key('unapproved_only'):
        data = data.filter(is_approved=False).distinct()
        params['unapproved_only'] = True

    # 3a  Whitelisted?
    if request.GET.has_key('whitelist_only'):
        data = data.filter(is_whitelisted=True).distinct()
        params['whitelist_only'] = True


    # 4 Predicted or binary?
    # todo - refactor
    if params['predicted']:
        if (font_class is not None) and (font_class != '0'):
            data = filter(lambda x: x.is_font_class_predicted(params['font_class']['name']), data)
        else:
            data = filter(lambda x: x.is_font_class_predicted(), data)


        # todo - hack - get all ids and get the font families again
        data = models.FontFamily.objects.filter(id__in=map(lambda x: x.id, data))


    # Sorting
    font_classes = sorted(models.FontClass.objects.all().filter(is_original=True), key=lambda x: not x.name.startswith('tk_'))
    if request.GET.has_key('sort_by'):
        font_class_sort_by = request.GET['sort_by']
    else:
        font_class_sort_by = font_classes[0].name

    if font_class_sort_by == '0': # alphabetical sort
        data = sorted(data, key=lambda x:x.name, reverse=params['descreasing_sort'])

        params['sort_by'] = {'id':'0','name':'0'}

    # todo - refactor
    elif params['prediction_sort']:
        data = filter(lambda x:x.is_font_class_predicted(), data) # only show fonts with predictions
        data = sorted(data, key=lambda x: x.font_class_predicted_value(font_class_sort_by), reverse=params['descreasing_sort'])
    else:
        data = sorted(data, key=lambda x: x.font_class_correction() == font_class_sort_by, reverse=params['descreasing_sort'])

    if font_class_sort_by != '0':
        fc = models.FontClass.objects.get(name=font_class_sort_by, is_original=True)
        params['sort_by'] = {'id': fc.id, 'name': fc.name}
        
    # todo - add correction for non tk class
    params['allow_non_tk_correction'] = font_class_sort_by.startswith("b_")

    # Paging
    paginator = Paginator(data, page_size)
    try:
        page = request.GET.get('page')
        data = paginator.page(page)
    except PageNotAnInteger:
        page = 1
        data = paginator.page(page)
    except EmptyPage:
        page = paginator.num_pages
        data = paginator.page(page)

    ret_params = {
        'data': data, 
        'font_classes': font_classes,
        'font_classes_corrected': models.FontClass.objects.all().filter(is_original=False), 
        'page_size': page_size,
        'font_classes_js': map(lambda x: str(x.name), font_classes) + ['0'],
        'enable_editing': settings.ENABLE_EDITING,
        'non_tk_correction_values': [-1, 0, 1] 
    }.copy()
    ret_params.update(params)
    
    return render(request, 'index.html', ret_params)

@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)

@csrf_exempt
def approve_single(request):
    if request.POST.has_key('ffamily_id'):
        x = request.POST['ffamily_id']
        ffamily = models.FontFamily.objects.get(id=x)
        ffamily.is_approved = True
        ffamily.save()
        return json_ok
    else:
        return JsonResponse({'status': '500', 'message': 'ffamily_id is not provided'})

@csrf_exempt
def approve_all(request):
    if request.POST.has_key('idList[]'):
        ids = request.POST.getlist('idList[]')
        models.FontFamily.objects.filter(id__in=ids).update(is_approved=True)
        return json_ok

@csrf_exempt
def unapprove_all(request):
    if request.POST.has_key('idList[]'):
        ids = request.POST.getlist('idList[]')
        models.FontFamily.objects.filter(id__in=ids).update(is_approved=False)
        return json_ok

@csrf_exempt
def font_correction(request):
    if request.POST.has_key('ffamily_id') and request.POST.has_key('font_class_id'):
        ff = models.FontFamily.objects.get(id=request.POST['ffamily_id'])
        ff.set_new_tk_font_class_correction(request.POST['font_class_id'])
        return json_ok

@csrf_exempt
def font_non_tk_correction(request):
    if request.POST.has_key('ffamily_id') and request.POST.has_key('font_class_name') and request.POST.has_key('value'):
        ff = models.FontFamily.objects.get(id=request.POST['ffamily_id'])
        if not ff:
            return JsonResponse({'status': '404', 'message': 'Font family is not found'})

        ff.set_non_tk_font_class_correction(request.POST['font_class_name'], request.POST['value'])
        return json_ok

@register.filter
def ffamily_correction_name(value):
    x = value.font_class_correction()
    return x.name if x else None

@register.filter
def ffamily_correction_id(value):
    x = value.font_class_correction()
    return x.id if x else None

@register.filter
def ffamily_is_whitelisted(value):
    x = value.is_whitelisted
    return "True" if x else "False"

@register.filter
def ffamily_font_class_predicted_value(value):
    return value.font_class_predicted_value()

@register.filter
def ffamily_font_class_predicted_value_by_class(current_font_family, sort_by_font_class_id):
    predict = current_font_family.font_class_predicted_value(models.FontClass.objects.get(id=sort_by_font_class_id).name)
    
    maybe_font_typekit_class_id = current_font_family.font_typekit_class()
    if maybe_font_typekit_class_id:
        font_typekit_class_id = maybe_font_typekit_class_id.id
    else:
        font_typekit_class_id = None

    if font_typekit_class_id != sort_by_font_class_id:
        if predict > models.FontFamily.prediction_threshold:
            return '<span style="color: red">{}</span>'.format(predict)
        else:
            return predict
    else: 
        if predict < models.FontFamily.prediction_threshold:
            return '<span style="color: red">{}</span>'.format(predict)
        else:
            return predict

@register.filter
def font_typekit_class(value):
    res = value.font_typekit_class()
    if res:
        return res.name
    else:
        return None

@register.filter
def font_other_classes(value):
    res = map(lambda x: x.name, value.font_other_classes())
    if res:
        return ', '.join(res)
    else:
        return None

@register.filter
def data_count(value):
    return len(value)

@register.assignment_tag
def get_font_class_non_tk_correction_status(font_family_id, non_tk_class_id):
    ff = models.FontFamily.objects.get(id=font_family_id)
    fc = models.FontClass.objects.get(id=non_tk_class_id)
    return ff.get_font_class_non_tk_correction_status(fc)



# import pdb; pdb.set_trace()
