from django.contrib import admin

# Register your models here.
from .models import *

admin.site.register(FontFamily)
admin.site.register(FontClass)
admin.site.register(FontClassValue)
admin.site.register(FontFace)
admin.site.register(FontPrediction)
