$(function() {

  //todo - refactor
  $('#btn_approve_all').click(function(e) {
    $.ajax({
      method: 'POST',
      url: 'approve_all',
      data: {'idList': getFontFamiliesIdList()}
    }).done(function(data) {
      showAlert('Approve all : success');
    }).fail(function(xhr, errmsg, err) {
      showAlert('Approve all : error');
    });
  }); 

  $('#btn_unapprove_all').click(function(e) {
    $.ajax({
      method: 'POST',
      url: 'unapprove_all',
      data: {'idList': getFontFamiliesIdList()}
    }).done(function(data) {
      showAlert('Unapprove all : success');
    }).fail(function(xhr, errmsg, err) {
      showAlert('Unapprove all : error');
    });
  }); 

  $('#font_families').on('click', '.btn_approve_font_family', function(e) {
    $.ajax({
      method: 'POST',
      url: 'approve_single',
      data: {'ffamily_id': $(this).data('ffamilyId')}
    }).done(function(data) {
      showAlert('Font approve single: success');
    }).fail(function(xhr, errmsg, err) {
      showAlert('Font approve single: error');
    });
  });

  var elements = {
    'chk_positive': {
      'name': 'positive', 
      're': 'positive=(false|true)'
    },
    'chk_predicted': {
      'name': 'predicted', 
      're': 'predicted=(false|true)'
    },
    'chk_corrected': {
      'name': 'corrected', 
      're': 'corrected=(false|true)'
    },
    'chk_unapproved_only': {
      'name': 'unapproved_only', 
      're': 'unapproved_only=(false|true)'
    },
    'chk_whitelist_only': {
      'name': 'whitelist_only', 
      're': 'whitelist_only=(false|true)'
    },
    'chk_prediction': {
      'name': 'prediction_sort', 
      're': 'prediction_sort=(false|true)'
    },
    'chk_decreasing': {
      'name': 'descreasing_sort', 
      're': 'descreasing_sort=(false|true)'
    }
  };

  //todo refactor
  for (var k in elements) {
    (function(k) {
      $('#' + k).click(function(e) {
        var re = new RegExp(elements[k]['re']);
        if (window.location.href.search('[?&]') === -1) {
          
          //chk_unapproved_only
          if (elements[k]['name'] === 'unapproved_only' && this.checked) {
            window.location.href =  window.location.href + '?unapproved_only=true';  
//          } else         
//          if (elements[k]['name'] === 'whitelist_only' && this.checked) {
//            window.location.href =  window.location.href + '?whitelist_only=true';  
          } else {
            window.location.href =  window.location.href + '?' + elements[k]['name'] + '=' + this.checked.toString();  
          }
        } else {

          //chk_unapproved_only
          if (elements[k]['name'] === 'unapproved_only') {
            if (this.checked) {
              window.location.href = window.location.href + '&unapproved_only=true';
            } else {
              var re = new RegExp("." + elements[k]['re']);
              window.location.href = window.location.href.replace(re, ''); //todo - remove ? or & before it
           }
          } else   
          if (elements[k]['name'] === 'whitelist_only') {
            if (this.checked) {
              window.location.href = window.location.href + '&whitelist_only=true';
            } else {
              var re = new RegExp("." + elements[k]['re']);
              window.location.href = window.location.href.replace(re, ''); //todo - remove ? or & before it
            }


          } else {
            if (re.test(window.location.href)) {
              window.location.href = window.location.href.replace(re, elements[k]['name'] + '=' + this.checked.toString());   
            } else {
              window.location.href = window.location.href + '&' + elements[k]['name'] + '=' + this.checked.toString();
            }
          }

        }
      }); 
    })(k);
  };

  $('#ddl_font_class').change(function(e) {
    var re = new RegExp('font_class=(' + fontClasses.join('|') + ')');
    if (window.location.href.search('[?&]') === -1) {
      window.location.href = window.location.href + '?font_class=' + this.value
    } else {
      if (re.test(window.location.href)) {
        window.location.href = window.location.href.replace(re, 'font_class=' + this.value);
      } else {
        window.location.href = window.location.href + '&font_class=' + this.value;
      }
    }
  });

  // todo refactor
  // Sort by
  $('#ddl_sort_by').change(function(e) {
    var re = new RegExp('sort_by=(' + fontClasses.join('|') + ')');
    if (window.location.href.search('[?&]') === -1) {
      window.location.href = window.location.href + '?sort_by=' + this.value
    } else {
      if (re.test(window.location.href)) {
        window.location.href = window.location.href.replace(re, 'sort_by=' + this.value);
      } else {
        window.location.href = window.location.href + '&sort_by=' + this.value;
      }
    }
  });




  $('.ddl_font_correction').change(function(e) {
    $.ajax({
      method: 'POST',
      url: 'font_correction',
      data: {'ffamily_id': $(this).data('ffamilyId'), 'font_class_id': this.selectedOptions[0].value}
      }).done(function(data) {
      showAlert('Correction : success');
    }).fail(function(xhr, errmsg, err) {
      showAlert('Correction : error');
    });
  });

  function getFontFamiliesIdList() {
    var idList = []; ;
    $.each($('.btn_approve_font_family'), function (index, item) {
      idList.push($(item).data('ffamilyId'));  
    });

    return idList;
  };

  function showAlert(msg) {
    var el = document.createElement("div");
    el.setAttribute("style","position:fixed;top:50%;left:40%;background-color:white;");
    el.innerHTML = '<b style="font-size: 30px; font-weight: bold;">' + msg + '</b>';
    setTimeout(function(){
      el.parentNode.removeChild(el);
    }, 1000);
    
    document.body.appendChild(el);
  }

  $('.ddl_font_non_tk_correction').change(function(e) {


    // debugger

    $.ajax({
      method: 'POST',
      url: 'font_non_tk_correction',
      data: {'ffamily_id': $(this).data('ffamilyId'), 'font_class_name': $('#ddl_sort_by').val(), 'value': this.selectedOptions[0].value}
      }).done(function(data) {
      showAlert('Correction : success');
    }).fail(function(xhr, errmsg, err) {
      showAlert('Correction : error');
    });
  });
});