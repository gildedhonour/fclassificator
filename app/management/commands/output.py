from django.core.management.base import BaseCommand, CommandError
import pandas as pd
import os
from app import models
from django.conf import settings
import shutil
import datetime

class Command(BaseCommand):
    help = ''
    can_import_settings = True

    def handle(self, *args, **options):

        backupDir = os.path.join(settings.INPUT_DATA_DIR, 'backup')

        # output approvals
        approved_file_name = os.path.join(settings.INPUT_DATA_DIR, 'approved_ui.hdf')

        # backup existing file
        if os.path.isfile(approved_file_name):
          backup = os.path.join(backupDir, 'approved_ui_' + datetime.datetime.now().isoformat() + '.hdf')
          shutil.move(approved_file_name,backup)

        fc_list = models.FontClass.objects.all().filter(is_original=False)
        data = {}
        for fc_list_item in fc_list:
          res1 = {}
          for ff_list_item in fc_list_item.fontfamily_set.all():
              res1[str(ff_list_item.name)] = int(ff_list_item.is_approved)

          data[fc_list_item.name] = res1

        df = pd.DataFrame(data, columns=map(lambda x: str(x.name), fc_list))
        df.to_hdf(approved_file_name, key='approved')


        # output corrections
        corrections_file_name = os.path.join(settings.INPUT_DATA_DIR, 'familyCorrections_ui.hdf')

        # backup existing file
        if os.path.isfile(corrections_file_name):
          backup = os.path.join(backupDir, 'familyCorrections_ui_' + datetime.datetime.now().isoformat() + '.hdf')
          shutil.move(corrections_file_name,backup)

        fc_list = models.FontClass.objects.all().filter(is_original=False)
        data = {}
        for fc_list_item in fc_list:
          res1 = {}
          for ff_list_item in fc_list_item.fontfamily_set.all():
              val = ff_list_item.fontclassvalue_set.all().filter(font_class=fc_list_item, font_family=ff_list_item).first().value
              res1[str(ff_list_item.name)] = val

          data[fc_list_item.name] = res1

        df = pd.DataFrame(data, columns=map(lambda x: str(x.name), fc_list)).fillna(0)
        df.to_hdf(corrections_file_name, key='familyCorrections')