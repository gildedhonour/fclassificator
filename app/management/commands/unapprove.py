from django.core.management.base import BaseCommand, CommandError
import h5py
import csv
import pandas
import json
from django.db.utils import IntegrityError
from app import models

class Command(BaseCommand):
    help = 'Unapproves all the font families'

    def handle(self, *args, **options):
        print('Unapproving all the font families...')
        models.FontFamily.objects.all().update(is_approved=False)
        print('Done')
