from django.core.management.base import BaseCommand, CommandError
import pandas
import json
from django.conf import settings
import os

from app import models

class Command(BaseCommand):
    help = 'Populates the db with the data stored in hdf and json files'
    can_import_settings = True

    def handle(self, *args, **options):
        self._truncate_all()
        self._insert_labels(self._get_path('familyLabels.hdf'), 'familyLabels', True)
        self._insert_families()
        self._insert_predictions()
        self._insert_labels(self._get_path('familyCorrections.hdf'), 'familyCorrections', False)
        print('\nDone!')

    def _get_path(self, file_name):
        return os.path.join(settings.INPUT_DATA_DIR, file_name)
        
    def _insert_labels(self, hdf_file_name, hdf_df_name, is_original):
        df = pandas.HDFStore(hdf_file_name)[hdf_df_name]
        for i, row in df.iteritems():
            for i2, row2 in row.iteritems():
                ff = models.FontFamily.objects.filter(name=i2).first()
                if not ff:
                    ff = models.FontFamily(name=i2, is_approved=False, url=None, is_typekit_family=False, is_whitelisted=False)
                    ff.save()
                    print('Font Family {}: inserted'.format(ff))
                
                fc = models.FontClass.objects.filter(name=i, is_original=is_original).first()
                if not fc:
                    fc = models.FontClass(name=i, is_original=is_original)
                    fc.save()
                    print('Font Class {}: inserted'.format(fc))

                ff.font_classes.add(fc)
                fcv = models.FontClassValue(value=row2, font_class=fc, font_family=ff)
                fcv.save()

    def _insert_families(self):
        with open(self._get_path('fontData.json')) as f:
            jdata = json.load(f)

        for font_family_item in jdata:
            family_data = jdata[font_family_item]
            url = family_data[2]
            is_typekit_family = (family_data[0] == 'TK')
            is_whitelisted = (family_data[1] == True)
            ffamily = models.FontFamily.objects.filter(name=font_family_item).first()
            if not ffamily:
                ffamily = models.FontFamily(name=font_family_item, is_approved=False, url=url, is_typekit_family=is_typekit_family,is_whitelisted=is_whitelisted)
            else:
                ffamily.url = url
                ffamily.is_typekit_family = is_typekit_family
                ffamily.is_whitelisted = is_whitelisted
           
            ffamily.save()
            print('\nFont family {}: inserted'.format(font_family_item))
            for font_face_item in family_data[3]:
                fface = models.FontFace(name=font_face_item, font_family=ffamily)
                fface.save()
                print('Font face {}: inserted'.format(font_face_item))

    def _insert_predictions(self):
        df = pandas.HDFStore(self._get_path('predictions.hdf'))['predictions']
        for i, row in df.iteritems():
            fclass = models.FontClass.objects.get(name=i)
            print('\n')
            for i2, row2 in row.iteritems():
                fface = models.FontFace.objects.filter(name=i2).first()
                if fface is None:
                    print('[NOT FOUND] The font face {} does not exist but the prediction for it does. Continuing...'.format(i2))
                    continue

                fprediction = models.FontPrediction(value=row2, font_face=fface, font_class=fclass)
                fprediction.save()
                print('Prediction for {}({}): inserted'.format(fface, i))

    def _truncate_all(self):
        print('Truncating all...')
        models.FontClass.objects.all().delete()
        models.FontFamily.objects.all().delete()
        models.FontClassValue.objects.all().delete()
        models.FontFace.objects.all().delete()
        models.FontPrediction.objects.all().delete()
        print('Done\n')


# import pdb; pdb.set_trace()